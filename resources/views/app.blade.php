<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    {!! Html::style('css/bootstrap.min.css') !!}
    {!! Html::style('css/style.css') !!}
    {!! Html::style('css/font-awesome.min.css') !!}
    {!! Html::style('css/jquery-ui.css') !!}
</head>
<body>
<div class="container">
    @yield('content')
</div>
</body>
@yield('footer')
{!! Html::script('js/jquery.min.js') !!}
{!! Html::script('js/jquery-ui.js') !!}
{!! Html::script('js/bootstrap.min.js') !!}
</html>